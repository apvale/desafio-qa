class Desconto

  def initialize(valor, quantidade)
    @valor = valor
    @quantidade = quantidade
  end

  def calcular_desconto(total_item)
    resultado = (total_item / @quantidade).floor * @valor
    resultado
  end

end

class RegraPreco

  def initialize(preco_original, *descontos)
    @preco_original = preco_original
    @descontos = descontos
  end

  def total_prod(quantidade)
    resultado = (quantidade * @preco_original) - desconto_por(quantidade)
  end

  def desconto_por(total_item)
  	contador = 0
  	@descontos.each do |desconto|
  		contador += desconto.calcular_desconto(total_item)
  	end
  	contador		 	
  end
end

RULES = {
  'A' => RegraPreco.new(50, Desconto.new(20, 3)),
  'B' => RegraPreco.new(30, Desconto.new(15, 2)),
  'C' => RegraPreco.new(20),
  'D' => RegraPreco.new(15),
}

class CheckOut

  def initialize(rules)
    @rules = rules
    @items = Hash.new
  end

  def scan(item)
    if @items[item]     
      @items[item] += 1
    else
      @items[item] = 0
      @items[item] += 1
    end 
  end

  def total
  	contador = 0
  	@items.each do |(item, quantidade)|
  		contador += preco_por(item, quantidade)
  	end
  	contador 	
  end

  def preco_por(item, quantidade)
    if @rules[item]
      @rules[item].total_prod(quantidade)
    end
  end

end